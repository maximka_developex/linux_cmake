#include <iostream>

#include "../hellolib/hello.h"

int main(int argc, char *argv[])
{
    std::cout << Hello().getMessage() << std::endl;

    return 0;
}
